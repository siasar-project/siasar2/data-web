import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import SectionHeader from 'components/common/SectionHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import CommunityScoreTree from 'components/community/CommunityScoreTree';
import IndicatorsList from 'components/common/IndicatorsList';
import JoinList from 'components/common/JoinList';
import InfoBox from 'components/common/InfoBox';

class Community extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      community: {
        location: [],
        systems: [],
        serviceProviders: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`communities/${this.state.id}`)
      .then(community => this.setState({ community }));
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <PageHeader title={t('community')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.community}
              entityName="community"
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('population')}
              value={this.state.community.population}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('households')}
              value={this.state.community.households}
              color="community"
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('schools')}
              value={this.state.community.schoolsCount}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('healthCenters')}
              value={this.state.community.healthCentersCount}
              style={{ backgroundColor: entityColors[0] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <CommunityScoreTree
              wsp={this.state.community.wsp}
              wshl={this.state.community.wshl}
              wssi={this.state.community.wssi}
              wsl={this.state.community.wsl}
              shl={this.state.community.shl}
              wsi={this.state.community.wsi}
              sep={this.state.community.sep}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.community.pictureUrl} />
          </Col>
          <Col sm="12" md="12" xl="6" className="d-flex">
            <PointMap
              latitude={this.state.community.latitude}
              longitude={this.state.community.longitude}
              score={this.state.community.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('system_plural')}
              items={this.state.community.systems}
              path="systems"
            />
          </Col>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('serviceProvider_plural')}
              items={this.state.community.serviceProviders}
              path="service-providers"
            />
          </Col>
        </Row>
        <section className="page-break">
          <SectionHeader title={t('individualIndicators')} />
          <Row>
            <Col md="12" lg="4">
              <IndicatorsList
                title={t('water')}
                items={[
                  { title: `${t('accesibility')}`, value: this.state.community.wslAcc },
                  {
                    title: `${t('effectiveWaterCoverage')}`,
                    value: this.state.community.wslAcc,
                  },
                  { title: `${t('continuity')}`, value: this.state.community.wslCon },
                  { title: `${t('serviceHours')}`, value: this.state.community.wslCon },
                  { title: `${t('seasonality')}`, value: this.state.community.wslSea },
                  { title: `${t('demandCoverage')}`, value: this.state.community.wslSea },
                  { title: `${t('waterQuality')}`, value: this.state.community.wslQua },
                  {
                    title: `${t('waterAnalysisResults')}`,
                    value: this.state.community.wslQua,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="4">
              <IndicatorsList
                title={t('sanitationHygieneService')}
                items={[
                  {
                    title: `${t('sanitationServiceLevel')}`,
                    value: this.state.community.shlSsl,
                  },
                  {
                    title: `${t('improvedSanitationServiceLevel')}`,
                    value: this.state.community.fis,
                  },
                  {
                    title: `${t('ownSanitationCoverage')}`,
                    value: this.state.community.fps,
                  },
                  {
                    title: `${t('personalHygiene')}`,
                    value: this.state.community.shlPer,
                  },
                  {
                    title: `${t('waterSoapUsage')}`,
                    value: this.state.community.fhp,
                  },
                  {
                    title: `${t('improvedSanitationUsage')}`,
                    value: this.state.community.fus,
                  },
                  {
                    title: `${t('homeHygiene')}`,
                    value: this.state.community.shlWat,
                  },
                  {
                    title: `${t('homeWaterManagement')}`,
                    value: this.state.community.shlWat,
                  },
                  {
                    title: `${t('communityHygiene')}`,
                    value: this.state.community.shlWat,
                  },
                  {
                    title: `${t('garbageCollection')}`,
                    value: this.state.community.ftb,
                  },
                  {
                    title: `${t('absenceOpenDefecation')}`,
                    value: this.state.community.fafl,
                  },
                ]}
              />
            </Col>
            <Col md="12" lg="4">
              <IndicatorsList
                title={t('serviceLevelPublicCenters')}
                items={[
                  {
                    title: `${t('waterSanitationHygiene')}`,
                    value: this.state.community.ecs,
                  },
                  {
                    title: `${t('improvedDrinkingSchool')}`,
                    value: this.state.community.ecsEag,
                  },
                  {
                    title: `${t('waterServiceSchool')}`,
                    value: this.state.community.ecsEag,
                  },
                  {
                    title: `${t('improvedDrinkingHealth')}`,
                    value: this.state.community.ecsCag,
                  },
                  {
                    title: `${t('waterServiceHealth')}`,
                    value: this.state.community.ecsCag,
                  },
                  {
                    title: `${t('improvedSanitationSchool')}`,
                    value: this.state.community.ecsShe,
                  },
                  {
                    title: `${t('sanitationHygieneSchool')}`,
                    value: this.state.community.ecsShe,
                  },
                  {
                    title: `${t('improvedSanitationHealth')}`,
                    value: this.state.community.ecsShs,
                  },
                  {
                    title: `${t('sanitationHygieneHealth')}`,
                    value: this.state.community.ecsShs,
                  },
                ]}
              />
            </Col>
          </Row>
        </section>
      </React.Fragment>
    );
  }
}

Community.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(Community);
