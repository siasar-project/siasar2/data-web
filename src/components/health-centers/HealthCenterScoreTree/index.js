import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class HealthCenterScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L117.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L392.5,180 Z" className="score-tree-line" />
        <g id="ecsCsa" transform="translate(255, 55)">
          <circle r="65" className={`score-tree-circle background-${calculateScore(this.props.ecsCsa)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsCsa)}`}>
            {t('ecsCsa')}
          </text>
        </g>
        <g id="ecsCagi" transform="translate(117.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.ecsCagi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsCagi)}`}>
            {t('ecsCagi')}
          </text>
        </g>
        <g id="ecsShsi" transform="translate(392.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.ecsShsi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsShsi)}`}>
            {t('ecsShsi')}
          </text>
        </g>
        <text x="50" y="320">
          <tspan x="29" dy="1.2em">{t('ecsCsa')}: {t('ecsEscDescription')}</tspan>
          <tspan x="29" dy="1.2em">{t('ecsCagi')}: {t('ecsEagiDescription')}</tspan>
          <tspan x="29" dy="1.2em">{t('ecsShsi')}: {t('ecsSheiDescription')}</tspan>
        </text>
      </ScoreTree>
    );
  }
}

HealthCenterScoreTree.propTypes = {
  ecsCsa: PropTypes.number,
  ecsCagi: PropTypes.number,
  ecsShsi: PropTypes.number,
  t: PropTypes.func.isRequired,
};

HealthCenterScoreTree.defaultProps = {
  ecsCsa: null,
  ecsCagi: null,
  ecsShsi: null,
};

export default translate()(HealthCenterScoreTree);
