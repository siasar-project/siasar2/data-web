/* eslint no-underscore-dangle: ["error", { "allow": ["_model", "_meta"] }] */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button, Card, CardImg, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import { translate } from 'react-i18next';
import SectionHeader from 'components/common/SectionHeader';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

import Api from 'lib/Api';
import './style.css';

class DownloadExcel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: [],
    };
  }

  componentDidMount() {
    Api.get('countries').then((countries) => {
      const country = countries.features.map((c) => {
        c.properties.url = 'http://www.globalsiasar.org/sites/default/files/excels/' +
        `${c.properties.iso_a2}.zip`;
        c.properties.imageUrl = `/img/flags/${c.properties.iso_a2}.png`;
        return c.properties;
      });
      this.setState({ country });
    });
  }

  render() {
    const { t } = this.props;
    return (
      <React.Fragment>
        <SectionHeader />
        <h3 className="text-primary">{t('downloadExcelByCountry')}</h3>
        <Row>
          {this.state.country.map((country, i) => (
            i < this.state.country.length &&
            <Col lg="3" sm="4" key={country.name}>
              <Card>
                <CardImg className="cardImg mx-auto" src={country.imageUrl} alt="Country's flag" />
                <CardBody>
                  <CardTitle className="text-center">{t(`countries.${country.iso_a2}`)}</CardTitle>
                  <CardSubtitle className="text-center">
                    <Button target="_blank" href={country.url}>
                      <FontAwesomeIcon icon={faChevronDown} />  {t('download')}
                    </Button>
                  </CardSubtitle>
                </CardBody>
              </Card>
            </Col>
          ))}
        </Row>
      </React.Fragment>
    );
  }
}

DownloadExcel.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(DownloadExcel);
