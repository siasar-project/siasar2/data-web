import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ListGroup, ListGroupItem } from 'reactstrap';

class CentersList extends Component {
  render() {
    return (
      <div className="mb-2 font-sm">
        {this.props.title &&
          <h5>{this.props.title}</h5>
        }
        <ListGroup>
          {this.props.items.map(item => (
            <ListGroupItem key={item.title} className="d-flex justify-content-between align-items-center">
              {item.title}
              <span><b>{item.data}</b></span>
            </ListGroupItem>
          ))}
        </ListGroup>
      </div>
    );
  }
}

CentersList.propTypes = {
  title: PropTypes.string,
  items: PropTypes.arrayOf(PropTypes.object).isRequired,
};

CentersList.defaultProps = {
  title: null,
};

export default CentersList;
