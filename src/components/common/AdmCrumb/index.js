import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Breadcrumb, BreadcrumbItem, Input, Button } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';

class AdmCrumb extends Component {
  constructor(props) {
    super(props);

    this.state = {
      adm: [],
      admOptions: [],
    };

    this.reset = this.reset.bind(this);
    this.update = this.update.bind(this);
  }

  componentDidMount() {
    this.update();
  }

  componentDidUpdate(prevProps) {
    if (this.props.country.code !== prevProps.country.code) {
      this.reset();
    }
  }

  async update(event) {
    const params = {
      where: {
        country: this.props.country.code,
      },
    };

    const adm = [...this.state.adm];

    if (event) {
      adm.push(event.target.value);
    }

    adm.forEach((value, index) => {
      params.where[`adm${index + 1}`] = value;
    });

    params.group = [`adm${adm.length + 1}`];
    params.order = params.group;
    params.attributes = params.group;

    const results = await Api.get(this.props.endpoint, params);

    const fullAdm = !results.length || !results[0][`adm${adm.length + 1}`];

    this.setState({
      adm,
      fullAdm,
      admOptions: results.map(item => item[`adm${adm.length + 1}`]),
    });

    this.props.onChange(adm, fullAdm);
  }

  reset(event) {
    const adm = event ? this.state.adm.slice(0, event.target.dataset.value) : [];
    this.setState({
      adm,
    }, this.update);
  }

  render() {
    const { t } = this.props;
    return (
      <Breadcrumb className="align-items-center bg-secondary text-dark">
        <BreadcrumbItem>
          <Button color="light" size="sm" href="#" onClick={this.reset} data-value="0">
            {this.props.country.name}
          </Button>
        </BreadcrumbItem>
        {this.state.adm.map((adm, i) => (
          i < this.state.adm.length &&
          <BreadcrumbItem key={adm}>
            <Button color="light" size="sm" href="#" onClick={this.reset} data-value={i + 1}>{adm}</Button>
          </BreadcrumbItem>
        ))}
        {!this.state.fullAdm &&
          <BreadcrumbItem className="d-flex align-items-center d-print-none">
            <Input
              key={this.state.adm.length + 1}
              type="select"
              bsSize="sm"
              onChange={this.update}
              defaultValue=""
              required
            >
              <option value="" disabled hidden>{t('select')}</option>
              {this.state.admOptions.map(admOption => (
                <option key={admOption} value={admOption}>{admOption}</option>
              ))}
            </Input>
          </BreadcrumbItem>
        }
      </Breadcrumb>
    );
  }
}

AdmCrumb.propTypes = {
  country: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  endpoint: PropTypes.string,
  t: PropTypes.func.isRequired,
};

AdmCrumb.defaultProps = {
  endpoint: 'communities',
};

export default translate()(AdmCrumb);
