import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavItem as BSNavItem } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class NavItem extends Component {
  render() {
    return (
      <BSNavItem>
        <NavLink exact={this.props.exact} to={this.props.to} className="pl-0 nav-link" activeClassName="active">
          <FontAwesomeIcon icon={this.props.icon} fixedWidth />
          <span className="d-none d-md-inline pl-2">{this.props.label}</span>
        </NavLink>
      </BSNavItem>
    );
  }
}

NavItem.propTypes = {
  exact: PropTypes.bool,
  icon: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

NavItem.defaultProps = {
  exact: false,
};

export default NavItem;
