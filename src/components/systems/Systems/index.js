import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';

import Entities from 'components/common/Entities';
import BarChart from 'components/common/charts/BarChart';
import SectionHeader from 'components/common/SectionHeader';

import { scoreColors, randomColors, scores, formatFloat } from 'lib/Utils';
import Api from 'lib/Api';

class Systems extends Component {
  constructor(props) {
    super(props);

    this.state = {
      country: null,
      supplyTypeChartLabels: [],
      supplyTypeChartData: null,
      supplyTypeScoreChartData: null,
    };

    this.countryChange = this.countryChange.bind(this);
  }

  update() {
    const query = { where: { score: { $ne: 'N' } }, percentage: true };
    if (this.state.country) query.where.country = this.state.country.code;

    Api.get('systems/supply-type-scores', query).then((results) => {
      const data = scores.map(score => Object.keys(results).map(supply => formatFloat(results[supply][score])));

      this.setState({
        supplyTypeChartLabels: Object.keys(results),
        supplyTypeScoreChartData: [
          { label: 'A', data: data[0] },
          { label: 'B', data: data[1] },
          { label: 'C', data: data[2] },
          { label: 'D', data: data[3] },
        ],
      });
    });

    Api.get('systems/by-supply-type', query).then((results) => {
      this.setState({
        supplyTypeChartData: [{ data: Object.values(results), backgroundColor: randomColors }],
      });
    });
  }

  countryChange(country) {
    this.setState({ country }, this.update);
  }

  render() {
    const { t } = this.props;

    return (
      <Entities entity="system" endpoint="systems" indicator="wsi" countryChange={this.countryChange}>
        <SectionHeader title={t('dataAnalysis')} />
        <Row>
          <Col sm="12" md="6" className="mb-2">
            <BarChart
              title={`${t('system_plural')} ${t('by')} ${t('supplyType')}`}
              subtitle="&nbsp;"
              data={{
                labels: this.state.supplyTypeChartLabels.map(type => t(type)),
                datasets: this.state.supplyTypeChartData,
              }}
              options={{
                scales: {
                  xAxes: [{ display: true, stacked: true, ticks: { minRotation: 55 } }],
                },
              }}
              colors={scoreColors()}
              legend={{ display: false }}
              height={200}
            />
          </Col>
          <Col sm="12" md="6" className="mb-2">
            <BarChart
              title={`${t('supplyType')} ${t('by')} ${t('score')}`}
              subtitle={t('percentageOverTotal')}
              data={{
                labels: this.state.supplyTypeChartLabels.map(type => t(type)),
                datasets: this.state.supplyTypeScoreChartData,
              }}
              colors={scoreColors()}
              legend={{ display: false }}
              options={{
                scales: {
                  xAxes: [{ display: true, stacked: true, ticks: { minRotation: 55 } }],
                  yAxes: [{ stacked: true, ticks: { max: 100 } }],
                },
              }}
              height={200}
            />
          </Col>
        </Row>
      </Entities>
    );
  }
}

Systems.propTypes = {
  t: PropTypes.func.isRequired,
};

export default translate()(Systems);
