import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class SystemList extends Component {
  render() {
    return (
      <EntityList
        entity="system"
        endpoint="systems"
        fields={['servedHouseholds', 'wsi']}
      />
    );
  }
}

export default SystemList;
