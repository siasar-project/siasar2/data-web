import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class SchoolList extends Component {
  render() {
    return (
      <EntityList
        entity="school"
        endpoint="schools"
        fields={['students', 'ecsEagi', 'ecsShei']}
      />
    );
  }
}

export default SchoolList;
