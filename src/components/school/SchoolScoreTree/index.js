import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class SchoolScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L117.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L392.5,180 Z" className="score-tree-line" />
        <g id="ecsEsc" transform="translate(255, 55)">
          <circle r="65" className={`score-tree-circle background-${calculateScore(this.props.ecsEsc)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsEsc)}`}>
            {t('ecsEsc')}
          </text>
        </g>
        <g id="ecsEagi" transform="translate(117.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.ecsEagi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsEagi)}`}>
            {t('ecsEagi')}
          </text>
        </g>
        <g id="ecsShei" transform="translate(392.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.ecsShei)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.ecsShei)}`}>
            {t('ecsShei')}
          </text>
        </g>
        <text x="50" y="320">
          <tspan x="29" dy="1.2em">{t('ecsEsc')}: {t('ecsEscDescription')} </tspan>
          <tspan x="29" dy="1.2em">{t('ecsEagi')}: {t('ecsEagiDescription')}</tspan>
          <tspan x="29" dy="1.2em">{t('ecsShei')}: {t('ecsSheiDescription')}</tspan>
        </text>
      </ScoreTree>
    );
  }
}

SchoolScoreTree.propTypes = {
  ecsEsc: PropTypes.number,
  ecsEagi: PropTypes.number,
  ecsShei: PropTypes.number,
  t: PropTypes.func.isRequired,
};

SchoolScoreTree.defaultProps = {
  ecsEsc: null,
  ecsEagi: null,
  ecsShei: null,
};

export default translate()(SchoolScoreTree);
