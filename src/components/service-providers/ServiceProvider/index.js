import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import { translate } from 'react-i18next';

import Api from 'lib/Api';
import { entityColors } from 'lib/Utils';

import PageHeader from 'components/common/PageHeader';
import ValueBox from 'components/common/ValueBox';
import Picture from 'components/common/Picture';
import PointMap from 'components/common/maps/PointMap';
import ServiceProviderScoreTree from 'components/service-providers/ServiceProviderScoreTree';
import JoinList from 'components/common/JoinList';
import InfoBox from 'components/common/InfoBox';
import IndicatorsList from 'components/common/IndicatorsList';

class ServiceProvider extends Component {
  constructor(props) {
    super(props);

    const { match: { params } } = props;

    this.state = {
      id: parseInt(params.id, 10),
      serviceProvider: {
        location: [],
        communities: [],
        systems: [],
        serviceProviders: [],
      },
    };
  }

  componentDidMount() {
    Api.get(`service-providers/${this.state.id}`)
      .then(serviceProvider => this.setState({ serviceProvider }));
  }

  render() {
    const { t } = this.props;
    const legalStatus = this.state.serviceProvider.legalStatus ? `${t('yes')}` : `${t('no')}`;
    return (
      <React.Fragment>
        <PageHeader title={t('serviceProvider')} />
        <Row>
          <Col>
            <InfoBox
              entity={this.state.serviceProvider}
              entityName="serviceProvider"
            />
          </Col>
        </Row>
        <Row>
          <Col sm="6" md="3">
            <ValueBox
              title={t('servedHouseholds')}
              value={this.state.serviceProvider.servedHouseholds}
              style={{ backgroundColor: entityColors[2] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('monthBilling')}
              value={this.state.serviceProvider.monthBilling}
              style={{ backgroundColor: entityColors[2] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('womenCount')}
              value={this.state.serviceProvider.womenCount}
              style={{ backgroundColor: entityColors[2] }}
            />
          </Col>
          <Col sm="6" md="3">
            <ValueBox
              title={t('legalized')}
              value={legalStatus}
              style={{ backgroundColor: entityColors[2] }}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="3">
            <ServiceProviderScoreTree
              sep={this.state.serviceProvider.sep}
              sepOrg={this.state.serviceProvider.sepOrg}
              sepOpm={this.state.serviceProvider.sepOpm}
              sepEco={this.state.serviceProvider.sepEco}
              sepEnv={this.state.serviceProvider.sepEnv}
            />
          </Col>
          <Col sm="12" md="6" xl="3">
            <Picture imgUrl={this.state.serviceProvider.pictureUrl} />
          </Col>
          <Col sm="12" md="12" xl="6" className="d-flex">
            <PointMap
              latitude={this.state.serviceProvider.latitude}
              longitude={this.state.serviceProvider.longitude}
              score={this.state.serviceProvider.score}
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('community_plural')}
              items={this.state.serviceProvider.communities}
              path="communities"
            />
          </Col>
          <Col sm="12" md="6" xl="6">
            <JoinList
              title={t('system_plural')}
              items={this.state.serviceProvider.systems}
              path="systems"
            />
          </Col>
        </Row>
        <Row>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('organizationManagement')}
              items={[
                {
                  title: `${t('administrativeFactor')}`,
                  value: this.state.serviceProvider.amsp,
                },
                {
                  title: `${t('operabilityFactor')}`,
                  value: this.state.serviceProvider.opefsp,
                },
                {
                  title: `${t('genderFactor')}`,
                  value: this.state.serviceProvider.genfsp,
                },
                {
                  title: `${t('accountabilityFactor')}`,
                  value: this.state.serviceProvider.ramafps,
                },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('operationMaintenance')}
              items={[
                {
                  title: `${t('generalApplication')}`,
                  value: this.state.serviceProvider.omf,
                },
                {
                  title: `${t('residualChlorineFactor')}`,
                  value: this.state.serviceProvider.resclf,
                },
                {
                  title: `${t('regulation')}`,
                  value: this.state.serviceProvider.regf,
                },
                {
                  title: `${t('micromeasurement')}`,
                  value: this.state.serviceProvider.microf,
                },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('economicFinancialManagement')}
              items={[
                {
                  title: `${t('billingEfficiency')}`,
                  value: this.state.serviceProvider.berf,
                },
                {
                  title: `${t('collectionEfficiencyRatio')}`,
                  value: this.state.serviceProvider.cerf,
                },
                {
                  title: `${t('profitability')}`,
                  value: this.state.serviceProvider.pro,
                },
                {
                  title: `${t('liquidityRatio')}`,
                  value: this.state.serviceProvider.lrf,
                },
                {
                  title: `${t('solvencyRatio')}`,
                  value: this.state.serviceProvider.srf,
                },
                {
                  title: `${t('debtServiceCoverageRatio')}`,
                  value: this.state.serviceProvider.dscrf,
                },
              ]}
            />
          </Col>
          <Col md="12" lg="3">
            <IndicatorsList
              title={t('enviromentalManagement')}
              items={[
                {
                  title: `${t('sanitationPromotionFactor')}`,
                  value: this.state.serviceProvider.espf,
                },
                {
                  title: `${t('preventiveActions')}`,
                  value: this.state.serviceProvider.paaf,
                },
                {
                  title: `${t('correctiveActions')}`,
                  value: this.state.serviceProvider.espf,
                },
              ]}
            />
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

ServiceProvider.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  }).isRequired,
  t: PropTypes.func.isRequired,
};

export default translate()(ServiceProvider);
