import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import './lib/i18next';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((<App />), document.getElementById('root'));

registerServiceWorker();
